package tp.location.privacy;

import tp.location.privacy.tools.Tools;


import static tp.location.privacy.mechanisms.KAnonymity.gridNoise;

public class main {
    public static void main(String[] a) {
        Tools.getCSVFile(gridNoise(3, Tools.getDataset(1)), "test_granularity_0.02-3-anonymity");
        System.out.println("It works :)");
    }
}
