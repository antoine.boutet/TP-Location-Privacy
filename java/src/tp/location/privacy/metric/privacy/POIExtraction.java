package tp.location.privacy.metric.privacy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;

import tp.location.privacy.dataset.Dataset;
import tp.location.privacy.tools.Tools;

public class POIExtraction {
	

	public static int diameter = 250;
	public static int duration = 30;
	public static double d2r = Math.PI / 180;
	public static int milli2minute = 1000*60;
	
	public static int NB = 0;
	
	public static SimpleDateFormat formatterWriteWithMinute = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
	
	public static  boolean isInt = true;
	public static HashSet<Integer> users = new HashSet<>();
	
	public static void main(String[] args) {

		if(args.length<3) {
			System.out.println("Usage: data diameter duration");
			System.exit(0);
		}
		diameter = Integer.parseInt(args[1]);
		duration = Integer.parseInt(args[2]);	
		String filename = args[0];
						
		ArrayList<Dataset> pointsOfInterest = identifyPOI(filename, filename.replaceAll(".csv", "")+"."+duration + "." + diameter+".poi.csv");
		System.out.println(String.format("There are %d points of interest", pointsOfInterest.size()));
	}
	
	private static ArrayList<Dataset> identifyPOI(String filename, String logFile) {

		ArrayList<Double> x = new ArrayList<Double>();
		ArrayList<Double> y = new ArrayList<Double>();
		ArrayList<Long> time = new ArrayList<Long>();

		ArrayList<String> initialLine = new ArrayList<String>();

		ArrayList<Dataset> datasets = new ArrayList<>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			String line;
			while ((line = br.readLine()) != null) {
				
				String l = line.substring(line.indexOf(",")+1);

				Double cood_x = Double.parseDouble(line.substring(0, line.indexOf(",")));

				//pass cood_x
				line = line.substring(line.indexOf(",")+1);

				Double cood_y = Double.parseDouble(line.substring(0, line.indexOf(",")));

				//pass timestamp
				line = line.substring(line.indexOf(",")+1);

				// date format
				Long t = 1000 * Long.parseLong(line);

				// first entry
				if(x.size()==0) {
					x.add(cood_x);
					y.add(cood_y);
					time.add(t);
					initialLine.add(l);
					continue;
				}
				
				
				// if still in the same POI
				if(Tools.getDistance(x.get(0), y.get(0), cood_x, cood_y) < diameter) {
					x.add(cood_x);
					y.add(cood_y);
					time.add(t);
					initialLine.add(l);
					final Dataset dataset = new Dataset(cood_x, cood_y, t);
					datasets.add(dataset);
				}
				// the new entry is outside the current POI
				else {
					
					// if the duration at the same location is too short
					if(((time.get(time.size()-1) - time.get(0))/milli2minute) < duration) {
						
						
						// check if new distance if ok
						while(Tools.getDistance(x.get(0), y.get(0), cood_x, cood_y) >= diameter) {
														
//							log(logFile,initialLine.get(0));
							
							x.remove(0);
							y.remove(0);
							time.remove(0);
							initialLine.remove(0);
							
							if(x.size()==0) {
								break;
							}
						}
						
					}
					// else valid POI
					else {
						
						String center = getCenter(x,y);
						long deltaT = time.get(time.size()-1) - time.get(0);
						log(logFile,center, time.get(0), deltaT, x.size());
						
						x.clear();
						y.clear();
						time.clear();
						initialLine.clear();
						
					}
					
					x.add(cood_x);
					y.add(cood_y);
					time.add(t);
					initialLine.add(l);
				}
				
//				System.out.println(uid + " " + t + " " + cood_x + " " + cood_y);
			}
			br.close();
			
			
			if(initialLine.size()>1) {
				String center = getCenter(x,y);
				long deltaT = time.get(time.size()-1) - time.get(0);
				log(logFile,center, time.get(0), deltaT, x.size());
			}
			else {
//				log(logFile,initialLine.get(0));
			}
			
			

			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return datasets;
	}
	

	/**
	 * Log poi
	 * 
	 * @param filename
	 * @param center
	 * @param t0
	 * @param deltaT
	 * @param size
	 */
	private static void log(String filename, String center, long t0, long deltaT, int size) {

		try {
			
			FileWriter fstream = new FileWriter(filename, true);
			BufferedWriter out = new BufferedWriter(fstream);
					
			out.write((t0/1000) + "," + center + ",poi," + (deltaT/milli2minute) + "," + size + "\n");
			out.close();
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * Get the center of a list of points
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	private static String getCenter(ArrayList<Double> x, ArrayList<Double> y) {
		
		Double averageX = 0.0;
		Double averageY = 0.0;		
		
		for(Double d : x) {
			averageX += d;
		}
		
		for(Double d : y) {
			averageY += d;
		}
		
		averageX /= x.size();
		averageY /= y.size();
		
		return (averageX + "," + averageY);
	}

}
