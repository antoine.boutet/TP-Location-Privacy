package tp.location.privacy.metric.utility;

import java.io.*;
import java.util.HashSet;

public class Recommendation {

    private static int getPlaceCount(String filename) throws IOException {
        HashSet<String> places = new HashSet<>();

        BufferedReader br = new BufferedReader(new FileReader(filename));
        String line;
        while ((line = br.readLine()) != null) {
            String[] array = line.split(",");
            for (int i = array.length - 1; array[i].indexOf('"') == -1 ; i--) {
                places.add(array[i]);
            }
        }
        return places.size();
    }

    public static void main(String[] args) {

        try {
            System.out.println(String.format("There are %d places", getPlaceCount(args[0])));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
