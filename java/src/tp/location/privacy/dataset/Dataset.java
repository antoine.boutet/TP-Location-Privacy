package tp.location.privacy.dataset;

public class Dataset {
    public double x; //Longitude
    public double y; //Latitude
    public long timestamp;

    public Dataset() {}

    public Dataset(double x, double y, long timestamp) {
        this.x = x;
        this.y = y;
        this.timestamp = timestamp;
    }
}
