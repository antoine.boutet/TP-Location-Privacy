package tp.location.privacy.tools;

import tp.location.privacy.dataset.Dataset;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Useful methods
 * 
 * @author aboutet
 *
 */
public class Tools {
	
	static double earthRadius = 6371000; //meters
	
	/**
	 * Get distance between two points
	 * 
	 * @param lat1
	 * @param lng1
	 * @param lat2
	 * @param lng2
	 * @return
	 */
	public static float getDistance(Double lat1, Double lng1, Double lat2, Double lng2) {
	    double earthRadius = 6371000; //meters
	    double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(lng2-lng1);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	               Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
	               Math.sin(dLng/2) * Math.sin(dLng/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    float dist = (float) (earthRadius * c);

	    return dist;
	}
	
	/**
	 * log a string in a file
	 * 
	 * @param filename
	 * @param line
	 */
	private static void log(String filename, String line) {

		try {
			
			FileWriter fstream = new FileWriter(filename, true);
			BufferedWriter out = new BufferedWriter(fstream);
					
			out.write(line + "\n");
			out.close();
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * read a dataset file
	 *
	 * @param id
	 */
	public static ArrayList<Dataset> getDataset(int id) {
		ArrayList<Dataset> r = new ArrayList<Dataset>();

        try (Stream<String> stream = Files.lines(Paths.get("data/" + id + ".csv"))) {
            stream.forEach(line -> {
                String[] p = line.split(",");
                Dataset d = new Dataset();
                d.timestamp = Long.parseLong(p[2]);
                d.x = Double.parseDouble(p[0]);
                d.y = Double.parseDouble(p[1]);
                r.add(d);
            });
        }

		catch (Exception e) { e.printStackTrace(); }

		return r;
	}
	
	public static double distanceToDeltaLatLong(double distance)
	{
		return (Math.toDegrees(distance)) / earthRadius;
	}

	public static void getCSVFile(List<Dataset> dataset, String filename) {
        List<String> lines = new ArrayList<String>();
        for(Dataset d: dataset)
            lines.add(d.x + "," + d.y + "," + d.timestamp);
        Path file = Paths.get("data/output/" + filename + ".csv");
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        }
        catch (Exception e) { e.printStackTrace(); }
    }

}
