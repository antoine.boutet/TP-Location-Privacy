package tp.location.privacy.mechanisms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import tp.location.privacy.dataset.Dataset;
import tp.location.privacy.tools.Tools;

public class Noise {
	
	public static void main(String[] a)
	{
		
		int deltaTime = 8*3600; // seconds
		int deltaDistance = 10*500; // meters
		
		File folder = new File("data");
		
		for(File f : folder.listFiles())
		{
			String filename = "data/" + f.getName().replaceAll(".csv","");
			
			ArrayList<Dataset> datasets = new ArrayList<>();
			
			try {
				BufferedReader br = new BufferedReader(new FileReader(filename + ".csv"));
				
				String line;
				while ((line = br.readLine()) != null) {

					Double cood_x = Double.parseDouble(line.substring(0, line.indexOf(",")));

					//pass cood_x
					line = line.substring(line.indexOf(",")+1);

					Double cood_y = Double.parseDouble(line.substring(0, line.indexOf(",")));

					//pass timestamp
					line = line.substring(line.indexOf(",")+1);

					// date format
					Long t = 1000 * Long.parseLong(line);
					
					Dataset dataset = new Dataset();
					dataset.x = cood_x;
					dataset.y = cood_y;
					dataset.timestamp = t;
					
					datasets.add(dataset);
				}
				
				br.close();
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			List<Dataset> datasetWithNoise = addGaussianNoiseToDatasets(datasets,deltaTime,deltaDistance);
			
			try {
				
				FileWriter fstream = new FileWriter("data_high-noise/" + f.getName(), true);
				BufferedWriter out = new BufferedWriter(fstream);
				
				for(Dataset d : datasetWithNoise)
				{
					out.write(d.x + "," + d.y + "," + d.timestamp + "\n");
				}
				
				out.close();
			
			} catch (FileNotFoundException e) {
				//e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public static List<Dataset> addGaussianNoiseToDatasets(List<Dataset> input, long deltaTime, long deltaDistance)
	{
		ArrayList<Dataset> datasetWithNoise = new ArrayList<Dataset>();
		
		for(Dataset d : input)
		{
			Dataset dWithNoise = new Dataset();
			dWithNoise.timestamp = (long) addGaussianNoise(d.timestamp,deltaTime);
			dWithNoise.x = addGaussianNoise(d.x,Tools.distanceToDeltaLatLong(deltaDistance));
			dWithNoise.y = addGaussianNoise(d.y,Tools.distanceToDeltaLatLong(deltaDistance));
			
			datasetWithNoise.add(dWithNoise);
		}
		
		return datasetWithNoise;
		
	}
	
	public static double addGaussianNoise(double data, double deltaMax)
	{
		Random randomGen = new Random();
		double randomGaussian = randomGen.nextGaussian();
		
		double dataWithNoise = data + randomGaussian*deltaMax;
		
		return dataWithNoise;
	}
}
