package tp.location.privacy.mechanisms;

import tp.location.privacy.dataset.Dataset;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class KAnonymity {

    private final static double X_GRID_GRANULARITY = 0.02; // En longitude
    private final static double Y_GRID_GRANULARITY = 0.02; // En latitude

    // Algo qui découpe l'espace selon une grille et ramène les points au centre de leur case correspondante
    // X_GRID_GRANULARITY : longueur selon les X des cases de la grille
    // Y_GRID_GRANULARITY : longueur selon les Y des cases de la grille
    public static List<Dataset> gridNoise(int k, List<Dataset> input) {
        double xGranularity = X_GRID_GRANULARITY;
        double yGranularity = Y_GRID_GRANULARITY;

        List<Dataset> latLongTimestampBounds = getMinMaxCoordinates(input);

        double longMarker = latLongTimestampBounds.get(0).x;
        double latMarker = latLongTimestampBounds.get(0).y;
        double timestampMarker = latLongTimestampBounds.get(0).timestamp;
        double longBound = latLongTimestampBounds.get(1).x;
        double latBound = latLongTimestampBounds.get(1).y;
        double timestampBound = latLongTimestampBounds.get(1).timestamp;

        List<List<Dataset>> datasetExtracted = new ArrayList<>();
        List<Dataset> kAnonymDataset = new ArrayList<>();

        while(longMarker < longBound) {
            while(latMarker < latBound) {
                List<Dataset> temp = isolateSpatialCoordinates(input, latMarker, xGranularity, longMarker, yGranularity);
                List<Dataset> modifiedDatasets = new ArrayList<>();
                for(Dataset dataset: temp) {
                    modifiedDatasets.add(getNewCoordinates(dataset));
                }
                datasetExtracted.add(modifiedDatasets);
                latMarker += yGranularity;
            }
            latMarker = latLongTimestampBounds.get(0).y;
            longMarker += xGranularity;
        }

        for(List<Dataset> list: datasetExtracted) {
            if(list.size() >= k) {
                kAnonymDataset.addAll(list);
                System.out.println(list.size());
            }
        }

        return kAnonymDataset;
    }

    // Renvoie la coordonnée altérée selon la grille
    public static Dataset getNewCoordinates(Dataset input) {
        int x = (int)(input.x / X_GRID_GRANULARITY);
        int y = (int)(input.y / Y_GRID_GRANULARITY);

        Dataset d = new Dataset();
        d.x = x * X_GRID_GRANULARITY + X_GRID_GRANULARITY / 2;
        d.y = y * Y_GRID_GRANULARITY + Y_GRID_GRANULARITY / 2;
        d.timestamp = input.timestamp;

        return d;
    }

    // Renvoie la coordonnée altérée selon la grille
    public static List<Dataset> getMinMaxCoordinates(List<Dataset> input) {
        double minLat, maxLat, minLong, maxLong;
        long minTimestamp, maxTimestamp;
        minLat = Integer.MAX_VALUE;
        maxLat = Integer.MIN_VALUE;
        maxLong = Integer.MIN_VALUE;
        minLong = Integer.MAX_VALUE;
        minTimestamp = Integer.MAX_VALUE;
        maxTimestamp = Integer.MIN_VALUE;

        for(Dataset dataset: input) {
            if(dataset.x > maxLong) {
                maxLong = dataset.x;
            } else if(dataset.x < minLong) {
                minLong = dataset.x;
            }
            if(dataset.y > maxLat) {
                maxLat = dataset.y;
            } else if(dataset.y < minLat) {
                minLat = dataset.y;
            }
            if(dataset.timestamp > maxTimestamp) {
                maxTimestamp = dataset.timestamp;
            } else if(dataset.timestamp < minTimestamp) {
                minTimestamp = dataset.timestamp;
            }
        }

        List<Dataset> result = new LinkedList<>();

        result.add(new Dataset(minLong, minLat, minTimestamp));
        result.add(new Dataset(maxLong, maxLat, maxTimestamp));

        return result;
    }

    // Supprimme l'ensemble des inputToDelete des input et renvoie la liste altérée
    public static List<Dataset> deleteCoordinate(List<Dataset> input, List<Dataset> inputToDelete) {
        ArrayList<Dataset> output = new ArrayList<Dataset>();

        for(Dataset d: input)
            if(!inputToDelete.contains(d))
                output.add(d);

        return output;
    }

    // Peut être utile pour isoler un ensemble de points de l'ensemble (isolation spatiale)
    // Peut être utilisée avec deleteCoordinate
    public static List<Dataset> isolateSpatialCoordinates(List<Dataset> input, double beginLat, double latLength, double beginLong, double longLength) {
        ArrayList<Dataset> output = new ArrayList<Dataset>();

        for(Dataset d: input)
            if(d.x >= beginLong && d.x <= beginLong + longLength && d.y >= beginLat && d.y <= beginLat + latLength)
                output.add(d);

        return output;
    }


    // Peut être utile pour isoler un ensemble de points de l'ensemble (isolation temporelle)
    // Peut être utilisée avec deleteCoordinate
    public static List<Dataset> isolateTemporalCoordinates(List<Dataset> input, double beginTimestamp, double timestampLength) {
        ArrayList<Dataset> output = new ArrayList<Dataset>();

        for(Dataset d: input)
            if(d.timestamp >= beginTimestamp && d.timestamp <= beginTimestamp + timestampLength)
                output.add(d);

        return output;
    }
}
